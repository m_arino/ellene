package com.twitter.tags.antama_ws.ellene;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Currency;

/**
 * Created by IntelliJ IDEA.
 * User: m_arino
 * Date: 12/01/27
 * Time: 22:51
 * To change this template use File | Settings | File Templates.
 */
public class ImageActivity extends Activity {
    
    private String Ids[];
    private Uri uris[];
    
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.image);

        getImageData();

        ImageView imageView = (ImageView) findViewById(R.id.mainImage);
        try{
            imageView.setImageBitmap(getBitmap(this.uris[0],this.Ids[0]));
        }catch(IOException e){
            Log.e("Ellene",e.getMessage());
            e.printStackTrace();
        }
    }

    private Bitmap getBitmap(Uri uri,String id) throws IOException{
        uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,id);
        BitmapFactory.Options mOptions = new BitmapFactory.Options();
        mOptions.inSampleSize = 10;
        Bitmap resizeBitmap = null;

        ContentResolver mContentResolver = getContentResolver();

        InputStream is = mContentResolver.openInputStream(uri);
        resizeBitmap = BitmapFactory.decodeStream(is,null,mOptions);
        return resizeBitmap;
    }
    
    private void getImageData(){
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String targetColumn[] = new String[2];
        targetColumn[0] = "_id";
        targetColumn[1] = "_data";
        Cursor c = this.managedQuery(uri,targetColumn,null,null,null);
        
        c.moveToFirst();
        int imageCount = c.getCount();

        this.uris = new Uri[imageCount];
        this.Ids = new String[imageCount];
        
        String tag;
        for(int i = 0;i<imageCount;i++){
            Ids[i] = String.valueOf(c.getInt(0));
            uris[i] = Uri.parse(c.getString(1));
            c.moveToNext();
        }
    }

}